# Final Assignment

There are two folders, one is called `web` and contains the frontend part of the project, the relevant part is in `index.html` and `script.js`, in the page there is a form where you can insert new items in the database, a table that shows the records in the database and each row has a button that performs an ajax call to the api to delete the record.

The second folder is called `backend` and contains the Shark class and three servlets, one for each action Insert, Select and Delete.

Maria Santos Moises
