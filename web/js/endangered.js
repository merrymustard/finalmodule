$(document).ready(function(){
  // When the document is ready do what's underneath

  console.log('page loaded 🙂')

  // var ajaxSettings = {
  //   dataType: 'json',
  //   type:'GET',
  //   url: "js/endangered-sharks.json"
  // }

  // $.ajax(ajaxSettings)
  // THOSE ARE THE SAME ⬇️⬆️
  $.ajax({
    dataType: 'json',
    type:'GET',
    url: "./js/endangered-sharks.json"
  }).done(function (sharksList){
    console.log(sharksList)

    // We create an empty string
    var rows = "";

    // We append the HTML into the string creating the markup
    // that is gonna be inserted in the table
    $.each(sharksList, function( key, object ) {
      console.log(key)
      console.log(object)

      rows = rows + "<div class='shark-row'>"
      rows = rows + "<div class='shark-cell'><p>" + object.name + "</p></div>"
      rows = rows + "<div class='shark-cell'>" + object.iucnStatus + "</div>"
      rows = rows + "<div class='shark-cell'>" + object.populationTrend + "</div>"
      rows = rows + "<div class='shark-cell'><img src='" + object.img + "' alt='" + object.name + "' /></div>"
      rows = rows + "</div>"
    })
    $('.shark-body').append(rows)

    // $.each(sharksList, function( key, object ) {
    //   console.log(key)
    //   console.log(object)
    //
    //   rows += "<tr>"
    //   rows += "<td><p class='sharkTitles'>" + object.name + "</p></td>"
    //   rows += "<td>" + object.iucnStatus + "</td>"
    //   rows += "<td>" + object.populationTrend + "</td>"
    //   rows += "<td><img src='" + object.img + "' alt='" + object.name + "' /></td>"
    //   rows += "</tr>"
    // })

  }).fail(function(){
    alert('A shark ate the page!!!!!')
  })

})
