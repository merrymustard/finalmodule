//modal
var infos = [
  "The term “cartilaginous fish” means that the structure of the animal’s body is formed of cartilage, instead of bone.  </br></br> Unlike the fins of bony fishes, the fins of cartilaginous fishes cannot change shape or fold alongside their body.</br></br>Even though sharks don't have a bony skeleton like many other fish, they are still categorized with other vertebrates.</br>This class is made up of about 1,000 species of sharks,skates and rays.",
  "Sharks come in a wide variety of shapes, sizes and even colors.</br>The largest shark and the largest fish in the world is the whale shark, which is believed to reach a maximum length of 59 feet. </br>The smallest shark is thought to be the dwarf lanternshark which is about 6-8 inches long.",
  "The teeth of sharks don’t have roots, so they usually fall out after about a week. However, sharks have replacements arranged in rows and a new one can move in within one day to take the old one’s place.</br></br>Sharks have five to 15 rows of teeth in each jaw,</br> with most having five rows.",
  "A shark has tough skin that is covered by dermal denticles, which are small plates covered with enamel, similar to that found on our teeth.",
  "Sharks have a lateral line system along their sides which detects water movements. This helps the shark find prey and navigate around other objects at night or when water visibility is poor.</br></br>The lateral line system is made up of a network of fluid-filled canals beneath the shark’s skin. Pressure waves in the ocean water around the shark vibrate this liquid. </br></br>This, in turn, is transmitted to jelly in the system, which transmits to the shark’s nerve endings and the message is relayed to the brain.",
  "Sharks need to keep water moving over their gills to receive necessary oxygen. Not all sharks need to move constantly, though.</br></br>Some sharks have spiracles, a small opening behind their eyes, that force water across the shark’s gills so the shark can be still when it rests. Other sharks do need to swim constantly to keep water moving over their gills and their bodies, and have active and restful periods rather than undergoing deep sleep like we do.</br></br>They seem to be “sleep swimming,” with parts of their brain less active while they remain swimming.",
  "Some shark species are oviparous, meaning they lay eggs. Others are viviparous and give birth to live young. Within these live-bearing species, some have a placenta like human babies do, and others do not.</br></br>In those cases, the shark embryos get their nutrition from a yolk sac or unfertilized egg capsules filled with yolk. In the sand tiger shark, things are pretty competitive. The two largest embryos consume the other embryos of the litter!</br></br>All sharks reproduce using internal fertilization, though, with the male shark using his 'claspers' to grasp the female and then he releases sperm, which fertilizes the female's oocytes.</br>The fertilized ova are packaged in an egg case and then eggs are laid or the egg develops in the uterus.",
  "While nobody seems to know the true answer, it is estimated that the whale shark, the largest shark species, can live up to 100-150 years, and many of the smaller sharks can live at least 20-30 years.",
  "Bad publicity around a few shark species has doomed sharks in general to the misconception that they are vicious man-eaters.</br></br>In fact, only 10 out of all the shark species are considered dangerous to humans. All sharks should be treated with respect, though, as they are predators, often with sharp teeth that could inflict wounds.",
  "Humans are a greater threat to sharks than sharks are to us. Many shark species are threatened by fishing or bycatch, amounting to the death of millions of sharks each year.</br></br>Compare that to shark attack statistics - while a shark attack is a horrifying thing, there are about 10 fatalities worldwide each year due to sharks. Since they are long-lived species and only have a few young at once, sharks are vulnerable to overfishing.</br></br>One threat is the wasteful practice of shark-finning, a cruel practice in which the shark's fins are cut off while the rest of the shark is thrown back in the sea."
]

var cards = document.querySelectorAll('.card')
var modal = document.querySelector('.modal')
var modalContent = document.querySelector('#modal-content')
var x = document.querySelector('.close')

// Assign an event listener on each card
cards.forEach(function(show) {
  // Execute this function when clicked
  show.onclick = function() {
    // Read data-info attribute
    var infoIndex = show.dataset.info
    // Select correspondant string in infos array
    // Insert string in modal HTML
    modalContent.innerHTML = infos[infoIndex]
    // open modal
    modal.classList.toggle('open')
  }
})

// close modal event & function
//x.onclick = closeModal

function closeModal() {
  modal.classList.remove('open')
}

//shark
var shark = document.getElementById('vecShark')

function changeShark() {
  this.src = "./img/sharkDashed.png"
}

function changeSharkBack() {
  this.src = "./img/sharkVec.png"
}

// call function on shark when event is fired
if (shark !== null) {
  shark.onmouseover = changeShark
  shark.onmouseout = changeSharkBack
}


//forms
//myfomr1
var myform = document.getElementById('myform1');

function processForm (e) {
  e.preventDefault();
  var fullName = $("#name").val();
  var emailValue = $("#email").val();
  var passValue = $("#password").val();
  console.log(fullName);
  console.log(emailValue);
  console.log(passValue);

  var dataToBeSent = {
    "fullNameIs": fullName,
    "email": emailValue,
    "pass": passValue
  }
  console.log(dataToBeSent)

  $.ajax({
   'dataType': 'json',
   'type':'POST',
   'url': "http://www.winelove.club/doc/html/testRestApi.php",
   'data': {obj: JSON.stringify(dataToBeSent)}
 })
 .done(function(data){
   console.log(data)
   alert("Form sent successfully");
 })
 .fail(function(er) {
   console.log('error', er);
   alert("An error occurred, sorry!");
 })
}

$(document).ready(function(){
  $('#myForm1').submit(processForm);
});
