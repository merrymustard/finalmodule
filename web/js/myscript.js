$(document).ready(function() {
  var baseUrl = 'http://localhost:8080/sharkStatusDatabase/'
  //addEventListener
  $("#myForm").submit(processForm);

  function processForm(e) {
    e.preventDefault();

    //get html reference
    var sharkName = $("#sharkName").val();
    var sharkStatus = $("#sharkStatus").val();


    $.ajax({
      dataType: 'json',
      type: "POST",
      url: baseUrl + "saveShark",
      data: {
        sharkName: sharkName,
        sharkStatus: sharkStatus
      }
    }).done(function(response) {

      if (response[0].connect) {
        $("#output").text("sharkName and sharkStatus correct!!");
        fetchTable()
      } else {
        $("#output").text("incorrect");
      }
    }).fail(function() {
      console.log("error......")
    });
  }

  function fetchTable () {
    $.ajax({
      dataType: 'json',
      type: 'GET',
      url: baseUrl + 'getSharks'
    }).done(function (response) {
      var sharkList = response.sharks

      $('#sharkTable tbody').empty()

      sharkList.forEach(function (item) {
        var rowString = '<tr>'
        rowString += '<td>' + item.idName + '</td>'
        rowString += '<td>' + item.nameshark + '</td>'
        rowString += '<td>' + item.statusShark + '</td>'
        rowString += '<td><button data-id="' + item.idName + '">X</button></td>'
        rowString += '</tr>'

        $('#sharkTable tbody').append(rowString)
      })

      $('table button').each(function () {
        $(this).click(deleteShark)
      })
    }).fail(function () {
      console.log("error......")
    })
  }

  function deleteShark () {
    var idName = $(this).data('id')

    $.ajax({
      dataType: 'json',
      type: 'POST',
      url: baseUrl + 'deleteShark',
      data: {
        idname: idName
      }
    }).done(function (response) {
      if (response === true) {
        fetchTable()
      } else {
        alert('Oh Snap!')
      }
    }).fail(function () {
      console.log("error......")
    })
  }

  fetchTable()
});
