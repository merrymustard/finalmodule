package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Shark {
	
	//attributes
	private int idname;
	private String sharkName;
	private String sharkStatus;

	//db connection
	private Connection conn = null;
	private Statement stmt = null;
	
	
	// constructor
	public Shark() {
		try {
			// register driver
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
			//connection
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost:8889/sharkstatus?user=root&password=root");
			
			//create statement
			this.stmt = this.conn.createStatement();
					
		}catch(Exception ex) {
			System.out.println("error on User contrsuction:" + ex.getMessage());
		}
	}

	
	
	//encapsulation
	public int getidname() {
		return idname;
	}


	public void setidname(int idname) {
		this.idname = idname;
	}


	public String getSharkName() {
		return sharkName;
	}


	public void setSharkName(String sharkName) {
		this.sharkName = sharkName;
	}


	public String getSharkStatus() {
		return sharkStatus;
	}


	public void setSharkStatus(String sharkStatus) {
		this.sharkStatus = sharkStatus;
	}

	
   //crud methods
	public boolean save(String sharkName, String sharkStatus) {
		try {	
			//SQL String
			String sql = "INSERT INTO nameSharks (idname,nameshark,statusShark) VALUES (NULL, '" + sharkName + "','" + sharkStatus +"')";
			//execute SQL String
			
			System.out.println(sql);
			//Execute query
			this.stmt.executeUpdate(sql);
			

			return true;
		}catch (Exception ex){
			System.out.println("error on save shark " + ex.getMessage());
			return false;
		}
	}
	public boolean delete (int idname) {
		try {
			String sql = "DELETE FROM nameSharks WHERE idname = " + idname;
			
			int rowAffected = this.stmt.executeUpdate(sql);
			
			if (rowAffected > 0) {
				return true;
			} else {
				throw new Exception("Error deleting data");
			}
		} catch (Exception ex){
			System.out.println("error on delete sharks " + ex.getMessage());
			return false;
		}
		
	}
	public  ArrayList<Shark> getSharks() {
		try {	
			//SQL String
			String sql = "SELECT * FROM nameSharks";
			//execute SQL String
			
			System.out.println(sql);
			
			//Execute query
			ResultSet rs = this.stmt.executeQuery(sql);
			
			ArrayList<Shark> sharks = new ArrayList<Shark>();
			
			while (rs.next()) {
				Shark item = new Shark();
				item.setidname(rs.getInt("idName"));
				item.setSharkName(rs.getString("nameshark"));
				item.setSharkStatus(rs.getString("statusShark"));
				
				sharks.add(item);
			}

			return sharks;
		}catch (Exception ex){
			System.out.println("error on get sharks " + ex.getMessage());
			return null;
		}
	}
	
}
