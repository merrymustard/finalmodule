

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import model.Shark;

/**
 * Servlet implementation class getShark
 */
@WebServlet("/getSharks")
public class getSharks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getSharks() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    		response.addHeader("Access-Control-Allow-Origin", "*");
    		
    		//CREATE OBJECT s with the name class
    		Shark s = new Shark();
    		
    		ArrayList<Shark> sharks= s.getSharks();
    		
    		JSONArray list = new JSONArray();
		for (Shark shark: sharks) {		
			JSONObject item = new JSONObject();
			item.put("idName", shark.getidname());
			item.put("nameshark", shark.getSharkName());
			item.put("statusShark", shark.getSharkStatus());
			
			list.add(item);  
		}
		
		//main root
		JSONObject mainRoot = new JSONObject();
		mainRoot.put("sharks",list);
    		
    		// TODO Auto-generated method stub
		response.getWriter().append(mainRoot.toJSONString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
